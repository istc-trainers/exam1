# Instructions

#### 1. Clone this repository to your local machine.

#### 2. In a feature branch, write functions that do the following:

Note: make sure you commit each time you finish the implementation of a function.

1. `sum(a, b) => Number`. A sum function that takes two numbers and return the sum of the two. E.g. `sum(2,3) => 5`. Commit your changes.
2. `sumList(list) => Number`. A sumList function that takes an Array of numbers and return the sum of all the values it contains. E.g. `sumList([2,3,4]) => 9`. Commit your changes.
3. `timer(start, end, interval) => undefined` which given a number as `start` and `end` will display an iterating value to the console every `interval` milliseconds.  

#### 3. Wrap the function definitions in a module and call them from an `index.js` file which requires that module.

Commit when you're done.

#### 4. Generate a new `package.json` and add a devDependancy to `jasmine` and a script named `test` that will execute the `jasmine` command.

Commit when you're done.

#### 5. Rebase your work against `origin/master` using, successively:

1. `git fetch origin`
2. `git rebase origin/master`

Make sure all your changes are under version control and force push to your own fork.

#### 6. From the *parent directory*, package your full working directory using the command `tar -cf firstname-lastname-exam.tar exam1` replacing the name with your first and last name and send the resulting file to `nicolas.wormser@gmail.com`.

PS: Thank you all for your participation!
